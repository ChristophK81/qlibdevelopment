/*
 * Detector.h
 *
 *  Created on: 29.09.2015
 *      Author: ck
 */

#ifndef SRC_DETECTOR_H_
#define SRC_DETECTOR_H_

#include <opencv2/opencv.hpp>
#include <opencv2/features2d.hpp>
#include <vector>
#include <iostream>

namespace qlib {

using namespace cv;
using namespace std;

class Detector {

private:
	const double nnMatchRatio = 0.8f;
	const double ransacThresh = 2.5f;
	const int bbMinInliers = 9;

protected:
    Ptr<ORB> detector;
    Ptr<DescriptorMatcher> matcher;
    Mat templateFrame, templateDesc;

    Mat detectImage;

    vector<KeyPoint> templateKp;
    vector<Point2f> templateBb;


	vector<Point2f> Points (vector<KeyPoint> keypoints);
	void drawBoundingBox(Mat image, vector<Point2f> bb);

public:
	Detector();
	virtual ~Detector();

	Mat process(const Mat frame);
	void setTemplate(const Mat image);
	Mat getSubMat(int minX, int maxX, int minY,int maxY);

	Mat getSerialH();
	Mat getSerialV();
	Mat getMagicNumber();

};

} /* namespace qlib */

#endif /* SRC_DETECTOR_H_ */
